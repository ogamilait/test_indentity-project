﻿using Identity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Identity.Controllers
{
    public class PostController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Post
        public ActionResult Index()
        {
            var listPosts = db.Posts.ToList();

            return View("Index", listPosts);
        }

        public ActionResult AddPost(Post model) {

            db.Posts.Add(model);

            db.SaveChanges();

            var listPosts = db.Posts.ToList();

            //var t1 = db.Posts.FirstOrDefault(x => x.Title == "First post");
            //var t2 = db.Posts.Where(x=>x.Title == "First post");
            //var t3 = db.Posts.Find(10);

            return View("Index", listPosts);
        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}